package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Cart;
import com.example.demo.entity.Product;
import com.example.demo.persistance.Cart_Repository;
@Service
public class Cart_Service_Implementation implements Cart_Service_Declarations{

	private Cart_Repository cartDao;
	
	@Autowired
	public Cart_Service_Implementation(Cart_Repository cartDao) {
		this.cartDao=cartDao;
	}

	@Override
	@Transactional
	public List<Cart> displayAllCart() {
		return cartDao.findAll();
	}
	
	@Override
	@Transactional
	public Cart displayCartById(int id) {
		return cartDao.findById(id).get();
	}

	@Override
	@Transactional
	public void insertCart(Cart p) {
		List<Cart> cart= cartDao.findCartByUserId(p.getUserId());
		for(Cart c:cart) {
			if(c.getUserId()== p.getUserId() && c.getProductId()==p.getProductId()) {
				return;
			}
		}
		cartDao.save(p);
		
	}

	@Override
	@Transactional
	public void updateCart(Cart p) {
		cartDao.save(p);
		
	}

	@Override
	@Transactional
	public void deleteCart(int id) {
		cartDao.deleteById(id);
		
	}
	@Override
	@Transactional
	public List<Product> getUserCart(int id) {
		return cartDao.findByUserId(id);
	}
	
	@Override
	@Transactional
	public void deleteUserCart(int id) {
		 cartDao.deleteByUserId(id);
	}
	@Override
	@Transactional
	public
	List<Cart> findCartByUserId(int userId) {
	return	cartDao.findCartByUserId(userId);
	}
}
