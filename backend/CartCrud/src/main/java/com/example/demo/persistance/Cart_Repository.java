package com.example.demo.persistance;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.entity.Cart;

public interface Cart_Repository extends JpaRepository<Cart, Integer> {
	@Query(value="select p from Product p join Cart c on p.productId=c.productId join User u on c.userId=u.userId where u.userId=?1 and c.userId=?1")
	List findByUserId(int userId);
	
	@Query(value="select c from Product p join Cart c on p.productId=c.productId join User u on c.userId=u.userId where u.userId=?1 and c.userId=?1")
	List<Cart> findCartByUserId(int userId);
	@Modifying
	@Query(value="delete from Cart where userId=?1")
	void deleteByUserId(int userId);
	
//	List<Cart> findByUserId(int userId);
}
