package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Cart;

public interface Cart_Service_Declarations {
	public List<Cart>displayAllCart();
	public Cart displayCartById(int id);
	public void insertCart(Cart p);
	public void updateCart(Cart p);
	public void deleteCart(int id);
	public List getUserCart(int id);
	public void deleteUserCart(int id);
	List findCartByUserId(int userId);
}
