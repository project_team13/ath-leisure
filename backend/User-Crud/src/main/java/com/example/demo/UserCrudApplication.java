package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import com.example.demo.service.User_Service_Implementation;

@SpringBootApplication
public class UserCrudApplication {
	@Autowired
	private User_Service_Implementation userservice;

	public static void main(String[] args) {
		SpringApplication.run(UserCrudApplication.class, args);
	}
	      @EventListener(ApplicationReadyEvent.class)
		  public void triggerMail() {
			userservice.sendEmail("springemailto7@gmail.com", "registration successfull", "registration");
			
		}
	}

