package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="User")
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int uId;
	@Column(name="Name")
	private String uName;
	@Column(name="address")
	private String address;
	@Column(name="Email")
	private String uEmail;
	@Column(name="Mobile")
	private long uMobile;
	@Column(name="Password")
	private String uPassword;
	//@Column(name="verificationcode",updatable=false)
	//private String verificationCode;
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User(int uId, String uName, String address, String uEmail, long uMobile, String uPassword) {
		super();
		this.uId = uId;
		this.uName = uName;
		this.address = address;
		this.uEmail = uEmail;
		this.uMobile = uMobile;
		this.uPassword = uPassword;
		//this.verificationCode=verificationCode;
	}
	
	
	public int getuId() {
		return uId;
	}
	public void setuId(int uId) {
		this.uId = uId;
	}
	public String getuName() {
		return uName;
	}
	public void setuName(String uName) {
		this.uName = uName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getuEmail() {
		return uEmail;
	}
	public void setuEmail(String uEmail) {
		this.uEmail = uEmail;
	}
	public long getuMobile() {
		return uMobile;
	}
	public void setuMobile(long uMobile) {
		this.uMobile = uMobile;
	}
	public String getuPassword() {
		return uPassword;
	}
	public void setuPassword(String uPassword) {
		this.uPassword = uPassword;
	}
	//}
	//public void setVerificationCode(String verificationCode) {
	//	this.verificationCode = verificationCode;
	//}
	@Override
	public String toString() {
		return "User [uId=" + uId + ", uName=" + uName + ", address=" + address + ", uEmail=" + uEmail + ", uMobile="
				+ uMobile + ", uPassword=" + uPassword +"]";
	}
}
