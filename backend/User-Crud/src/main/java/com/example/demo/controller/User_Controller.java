package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.User;
import com.example.demo.service.User_Service_Implementation;

import net.bytebuddy.utility.RandomString;

@RestController
@RequestMapping("/user")
@CrossOrigin()
public class User_Controller {
	
	private User_Service_Implementation usr;
	
	@PostMapping("/login")
	public ResponseEntity<?> loginUser(@RequestBody User u){
		User user=usr.displayUserById(u.getuId());
		if(user.getuPassword().equals(u.getuPassword())) {
			return ResponseEntity.ok(user);
		}
		return (ResponseEntity<?>) ResponseEntity.internalServerError();
	}
	
	@Autowired
	public User_Controller(User_Service_Implementation usr) {
		this.usr = usr;
	}
	
	@DeleteMapping("/deleteuser/{Id}")
	public void deleteUser(@PathVariable("Id") int id) {
		usr.deleteUser(id);		
	}
	
	@PostMapping("/insertuser")
	public void insertUser(@RequestBody User u) {
		usr.sendEmail(u.getuEmail(),"registration successfull", "sent");
		usr.insertUser(u);	
	}
	
	@PutMapping("/updateuser")
	public void updateUser(@RequestBody User u) {
		usr.updateUser(u);		
	}
	
	@GetMapping("/displayAllUsers")
	public List<User> displayAllUsers(){
		return usr.displayAllUsers();
	}
	
	@GetMapping("/displayUserById/{Id}")
	public User displayUserById(@PathVariable("Id") int id) {
		return usr.displayUserById(id);		
	}
}
