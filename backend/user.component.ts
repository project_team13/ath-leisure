import { Component } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {
     users:any;
  constructor() {
    this.users = [
    {userId:101, user_Name:'Pranaya', user_Address:'Hyderabad',email:'p@gmail.com',mobile:'8642098549',password:'p123'},
    {userId:102, user_Name:'Pranusha', user_Address:'Nirmal',email:'kp@gmail.com',mobile:'8753219065',password:'kp123'},
    {userId:103, user_Name:'Ankitha', user_Address:'Adilabad',email:'a@gmail.com',mobile:'8609834567',password:'a123'}
    ];
  }
}
