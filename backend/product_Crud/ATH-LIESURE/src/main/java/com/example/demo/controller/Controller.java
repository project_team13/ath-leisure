package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Product;
import com.example.demo.service.Product_Service_Implementations;

@RestController
@RequestMapping("/product")
public class Controller {
	
	private Product_Service_Implementations psi;


	
	@Autowired
	public Controller(Product_Service_Implementations psi) {
		super();
		this.psi = psi;
	}
	
	@GetMapping("/list")
	public List<Product> displayAllProducts() {
		
		return psi.displayAllProducts();
	}
	
	@GetMapping("/list/{id}")
	public Product displayProductById(@PathVariable("pid") int id) {
		return psi.displayProductById(id);
	}
	
	@PostMapping("/insertProduct")
	public void insertProduct(@RequestBody Product p) {
		psi.insertProduct(p);
	}
	
	
	@PutMapping("/updateProduct")
	public void updateProduct(@RequestBody Product p) {
		psi.updateProduct(p);
	}
	
	@DeleteMapping("/deleteProduct/{id}")
	public void deleteProduct(@PathVariable("pid") int id) {
		psi.deleteProduct(id);
		
	}
	
	

}
