package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Product;

public interface Product_Service_Declarations {

	public List<Product> displayAllProducts();
	public Product displayProductById(int id);
	public void insertProduct(Product p);
	public void updateProduct(Product p);
	public void deleteProduct(int id);
	
	
	
	
}
