package com.example.demo.persistance;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.entity.Product;
import com.example.demo.entity.WishList;

public interface WishList_Repository extends JpaRepository<WishList, Integer>{

	@Query(value="select p from Product p join WishList c on p.productId=c.productId join User u on c.userId=u.userId where u.userId=?1 and c.userId=?1")
	List<Product>findByUserId(int userId);
	
	@Query(value="select c from Product p join WishList c on p.productId=c.productId join User u on c.userId=u.userId where u.userId=?1 and c.userId=?1")
	List<WishList> findByWishlistUserId(int userId);
	
	@Modifying
	@Query(value="delete from WishList  where userId=?1")
	void  deleteByUsersId(int userId);
	
//	@Modifying
//	@Query(value="delete from WishList  where userId=?1")
//	void  deleteByUsersId(int userId);
}
