package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Product;
import com.example.demo.entity.WishList;
import com.example.demo.persistance.WishList_Repository;

@Service
public class WishList_Service_Implementation implements WishList_Service_Declarations{

	private WishList_Repository wishListDao;
	@Autowired
	public WishList_Service_Implementation(WishList_Repository wishListDao) {
		super();
		this.wishListDao = wishListDao;
	}
	@Override
	@Transactional
	public List<WishList> display() {
		// TODO Auto-generated method stub
		return wishListDao.findAll();
	}
	@Override
	@Transactional
	public void delete(int id) {
		// TODO Auto-generated method stub
		wishListDao.deleteById(id);
	}
	@Override
	@Transactional
	public void insert(WishList w) {
		// TODO Auto-generated method stub
		wishListDao.save(w);
	}
	@Override
	@Transactional
	public List<WishList> getWishlistByUserID(int id) {
		return wishListDao.findByWishlistUserId(id);
	}
	@Override
	@Transactional
	public List<Product> getProductByUserID(int id) {
		return wishListDao.findByUserId(id);
	}

	@Override
	@Transactional
	public void deleteUserWishlist(int id) {
		wishListDao.deleteByUsersId(id);
	}
}
