package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Product;
import com.example.demo.entity.WishList;

public interface WishList_Service_Declarations {

	List<WishList> display();
	public void insert(WishList w);
	void delete(int id);
	List<Product> getProductByUserID(int id);
	List<WishList> getWishlistByUserID(int id);
	void deleteUserWishlist(int id);
}
