package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Product;
import com.example.demo.entity.WishList;
import com.example.demo.service.WishList_Service_Declarations;

@RestController
@CrossOrigin()
@RequestMapping("/wishlist")

public class WishListController {
	
	private WishList_Service_Declarations wishListServ;

	@Autowired
	public WishListController(WishList_Service_Declarations wishListServ) {
		super();
		this.wishListServ = wishListServ;
	}
	@GetMapping("/list")
	public List<WishList> display(){
		return wishListServ.display();
	}
	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable("id") int id) {
		wishListServ.delete(id);
	}
	@PostMapping("/insert")
	public void insert(@RequestBody WishList w) {
		wishListServ.insert(w);
	}
	
	@GetMapping("/wlist/{id}")
	public List<WishList> displayWishlistByUserId(@PathVariable("id")int id){
		return wishListServ.getWishlistByUserID(id);
	}
	@GetMapping("/plist/{id}")
	public List<Product> displayProductByUserId(@PathVariable("id")int id){
		return wishListServ.getProductByUserID(id);
	}
	@DeleteMapping("/deletewishlist/{id}")
	public void deleteCart(@PathVariable("id") int id) {
		wishListServ.deleteUserWishlist(id);
		
	}
	
	 	
}
