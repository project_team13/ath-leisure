package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="wishlist")
public class WishList {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="wishList_Id")
	private int wishlistId;
	@Column(name="productId")
	private Integer productId;
	@Column(name="userId")
	private Integer userId;
	
	public WishList() {
		super();
		// TODO Auto-generated constructor stub
	}
	public WishList(int wishlistId, Integer productId, Integer userId) {
		super();
		this.wishlistId = wishlistId;
		this.productId = productId;
		this.userId = userId;
	}
	@Override
	public String toString() {
		return "WhishList [wishlistId=" + wishlistId + ", pId=" + productId + ", uId=" + userId + "]";
	}
	public int getWishlistId() {
		return wishlistId;
	}
	public void setWishlistId(int wishlistId) {
		this.wishlistId = wishlistId;
	}
	public Integer getproductId() {
		return productId;
	}
	public void setproductId(Integer pId) {
		this.productId = pId;
	}
	public Integer getuserId() {
		return userId;
	}
	public void setuserId(Integer userId) {
		this.userId = userId;
	}
	

}
