package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.User;

public interface User_Service_Declaration {
	public void deleteUser(int id);
	public void insertUser(User u);
	public void updateUser(User u);
	public List<User> displayAllUsers();
	public User displayUserById(int id);
	public void sendEmail(String toEmail, String body, String subject);
}
