package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.persistance.User_Repository;

@Service
public class User_Service_Implementation implements User_Service_Declaration{
	private User_Repository ur;
	
	@Autowired
    private JavaMailSender mailSender;
	
    @Autowired
	public User_Service_Implementation(User_Repository ur) {
		this.ur = ur;
	}

	@Override
	public void deleteUser(int id) {
		// TODO Auto-generated method stub
		ur.deleteById(id);
		
	}

	@Override
	public void insertUser(User u) {
		// TODO Auto-generated method stub
		ur.save(u);
		
	}

	@Override
	public void updateUser(User u) {
		// TODO Auto-generated method stub
		ur.save(u);
		
	}

	@Override
	public List<User> displayAllUsers() {
		// TODO Auto-generated method stub
		return ur.findAll();
	}

	@Override
	public User displayUserById(int id) {
		// TODO Auto-generated method stub
		return ur.findById(id).get();
	}
	

	
	@Override
	public void sendEmail(String toEmail,String body,String subject) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(toEmail);
		message.setText(body);
	    message.setSubject(subject);
		message.setFrom("springangularfrom@gmail.com");
		mailSender.send(message);
		System.out.println("mail sent");		
	}
}
