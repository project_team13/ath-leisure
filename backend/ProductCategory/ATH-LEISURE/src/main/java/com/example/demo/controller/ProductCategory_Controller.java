package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.ProductCategory;
import com.example.demo.service.ProductCategory_Service_Implementations;

@RestController
@RequestMapping("/ProductCategory")
public class ProductCategory_Controller
{
	private ProductCategory_Service_Implementations pcser;
	@Autowired
	public ProductCategory_Controller(ProductCategory_Service_Implementations pcser) 
	{
		this.pcser = pcser;
	}
	@GetMapping("/list")
	public List<ProductCategory> displayAll() 
	{
		return pcser.displayAll();
	}
	@GetMapping("/list/{ProductCategoryId}")
	public ProductCategory displayBasedOnId(@PathVariable("ProductCategoryId")int pcId) 
	{
		return pcser.displayBasedOnId(pcId);
	}
	@PostMapping("/list")
	public void insert(@RequestBody ProductCategory pc) 
	{
		pcser.insert(pc);
	}
	@PutMapping("/list")
	public void update(@RequestBody ProductCategory pc)
	{
		pcser.update(pc);
	}
	@DeleteMapping("/list/{ProductCategoryId}")
	public void deleteById(@PathVariable("ProductCategoryId")int pcId)
	{
		pcser.deleteById(pcId);
	}
	
}
