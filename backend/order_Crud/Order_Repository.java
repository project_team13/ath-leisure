package com.example.demo.persistance;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.entity.Orders;

public interface Order_Repository extends JpaRepository<Orders, Integer> {
	@Query(value="from Orders  where userId=?1")
	List<Orders> findByUsersId(int userId);

	@Modifying
	@Query(value="delete from Orders  where userId=?1")
	void  deleteByUsersId(int userId);
}
