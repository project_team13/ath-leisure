package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Orders;
import com.example.demo.persistance.Order_Repository;

@Service
public class Order_Service_Implementation implements Order_Service_Declarations{
	@Autowired
	private Order_Repository orderDao;
	@Override
	@Transactional
	public List<Orders> diaplay(int uId) {
		// TODO Auto-generated method stub
		return orderDao.findByUsersId(uId);
	}
	
	@Override
	@Transactional
	public List<Orders>displayAll(){
		return orderDao.findAll();
	}
	@Override
	@Transactional
	public void updateOrders(Orders o){
		orderDao.save(o);
	}
	
	@Override
	@Transactional
	public void insertOrders(Orders o) {
		orderDao.save(o);
	}
	@Override
	@Transactional
	public void deleteOrder(int id) {
		orderDao.deleteByUsersId(id);
	}
}
