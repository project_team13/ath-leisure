package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Cart;
import com.example.demo.entity.Product;
import com.example.demo.service.Cart_Service_Implementation;

@RestController
@CrossOrigin()
@RequestMapping("/cart")
public class Cart_Controller {

	private Cart_Service_Implementation csi;
	@Autowired
	public Cart_Controller(Cart_Service_Implementation csi) {
		this.csi=csi;
		
	}
	@GetMapping("/list")
	public List<Cart> displayCart() {
		return csi.displayAllCart();
	}
	
	@GetMapping("/list/{id}")
	public Cart displayCarttById(@PathVariable("id") int id) {
		return csi.displayCartById(id);
	}
	
	@PostMapping("/insert")
	public void insertCart(@RequestBody Cart p) {
		csi.insertCart(p);
	}
	
	
	@PutMapping("/update")
	public void updateCart(@RequestBody Cart p) {
		csi.updateCart(p);
	}
	
	@DeleteMapping("/deletecart/{id}")
	public void deleteCart(@PathVariable("id") int id) {
		csi.deleteCart(id);
		
	}
	@GetMapping("/product/{id}")
	public List<Product> displayProductCart(@PathVariable("id")int id) {
		return csi.getUserCart(id);
	}
	@GetMapping("/cart/{id}")
	public List<Cart> displayUserCart(@PathVariable("id")int id) {
		return csi.findCartByUserId(id);
	}
	@DeleteMapping("/cart/{id}")
	public void deleteCartByUser(@PathVariable("id")int id) {
		csi.deleteUserCart(id);
	}
	
}
