package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="User")
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int uId;
	@Column(name="Name")
	private String uName;
	@Column(name="Loc")
	private String uLoc;
	@Column(name="Email")
	private String uEmail;
	@Column(name="Mobile")
	private int uMobile;
	@Column(name="Password")
	private String uPassword;
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User(int uId, String uName, String uLoc, String uEmail, int uMobile, String uPassword) {
		super();
		this.uId = uId;
		this.uName = uName;
		this.uLoc = uLoc;
		this.uEmail = uEmail;
		this.uMobile = uMobile;
		this.uPassword = uPassword;
	}
	public int getuId() {
		return uId;
	}
	public void setuId(int uId) {
		this.uId = uId;
	}
	public String getuName() {
		return uName;
	}
	public void setuName(String uName) {
		this.uName = uName;
	}
	public String getuLoc() {
		return uLoc;
	}
	public void setuLoc(String uLoc) {
		this.uLoc = uLoc;
	}
	public String getuEmail() {
		return uEmail;
	}
	public void setuEmail(String uEmail) {
		this.uEmail = uEmail;
	}
	public int getuMobile() {
		return uMobile;
	}
	public void setuMobile(int uMobile) {
		this.uMobile = uMobile;
	}
	public String getuPassword() {
		return uPassword;
	}
	public void setuPassword(String uPassword) {
		this.uPassword = uPassword;
	}
	@Override
	public String toString() {
		return "User [uId=" + uId + ", uName=" + uName + ", uLoc=" + uLoc + ", uEmail=" + uEmail + ", uMobile="
				+ uMobile + ", uPassword=" + uPassword + "]";
	}
}
