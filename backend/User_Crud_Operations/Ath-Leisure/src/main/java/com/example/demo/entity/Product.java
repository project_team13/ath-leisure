package com.example.demo.entity;

public class Product {
	private int pId;
	private String pName;
	private int pPrice;
	private String pDescription;
	private int pcCid;
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Product(int pId, String pName, int pPrice, String pDescription, int pcCid) {
		super();
		this.pId = pId;
		this.pName = pName;
		this.pPrice = pPrice;
		this.pDescription = pDescription;
		this.pcCid = pcCid;
	}
	public int getpId() {
		return pId;
	}
	public void setpId(int pId) {
		this.pId = pId;
	}
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public int getpPrice() {
		return pPrice;
	}
	public void setpPrice(int pPrice) {
		this.pPrice = pPrice;
	}
	public String getpDescription() {
		return pDescription;
	}
	public void setpDescription(String pDescription) {
		this.pDescription = pDescription;
	}
	public int getPcCid() {
		return pcCid;
	}
	public void setPcCid(int pcCid) {
		this.pcCid = pcCid;
	}
	@Override
	public String toString() {
		return "Product [pId=" + pId + ", pName=" + pName + ", pPrice=" + pPrice + ", pDescription=" + pDescription
				+ ", pcCid=" + pcCid + "]";
	}	
}
