import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private httpClient:HttpClient) {

  }
  getUserCartProduct(id:any){
    return this.httpClient.get('http://localhost:8080/cart/product/'+id);
  }
  getUserCart(id:any){
    return this.httpClient.get('http://localhost:8080/cart/cart/'+id);
  }
 addToCart(p:any){
   return this.httpClient.post( `http://localhost:8080/cart/insert`,p);
  }
  updateCart(c:any){
   return this.httpClient.put(`http://localhost:8080/cart/update`,c);
  }
  deleteCart(c:any){
    return this.httpClient.delete('http://localhost:8080/cart/deletecart/'+c);
  }

  getCartName(id:any){
  return this.httpClient.get('http://localhost:8080/product/cartname/'+id);
  }

}
