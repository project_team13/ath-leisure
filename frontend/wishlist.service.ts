import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WishlistService {
  constructor(private httpClient:HttpClient) { }
  deleteWishList(c:any){
    return this.httpClient.delete('http://localhost:8080/wishlist/delete/'+c);
   }
   getWishlist(id:any){
return this.httpClient.get('http://localhost:8080/wishlist/wlist/'+id);
   }
   getProduct(id:any){
    return this.httpClient.get('http://localhost:8080/wishlist/plist/'+id);
       }

   addtoWishlist(w:any){
    return this.httpClient.post('http://localhost:8080/wishlist/insert',w);
   }
}
