import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: any;
  constructor() 
  { 
    this.user=[
      {userId:11,uName:'abc',address:'hyd',uEmail:'xyz@gmail.com',uMobile:12345,uPassword:'user'},
      {userId:12,uName:'abc',address:'hyd',uEmail:'xyz@gmail.com',uMobile:12345,uPassword:'user'},
      {userId:13,uName:'abc',address:'hyd',uEmail:'xyz@gmail.com',uMobile:12345,uPassword:'user'},
      {userId:14,uName:'abc',address:'hyd',uEmail:'xyz@gmail.com',uMobile:12345,uPassword:'user'}
    ];
  }
  ngOnInit(): void {
  }
}
