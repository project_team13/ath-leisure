import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) {
  }
adminLoggedIn():boolean{
if(localStorage.getItem("admin")!=null){
  return true;
}
return false;
}
  userLoggedIn():boolean{
    if(localStorage.getItem("user")!=null){
      return true;
    }
    return false;
  }
  getUser(){
    return this.http.get('http://localhost:8080/user/displayAllUsers');
  }
  getUserById(id:any)
  {
     return this.http.get('http://localhost:8080/user/displayUserById/'+id);
  }
  updateUser(user:any){
    return this.http.put('http://localhost:8080/user/updateuser/', user);
  }
  deleteUser(id:any){
  return this.http.delete('http://localhost:8080/user/deleteuser/'+id);
  }
  deleteOrder(id:any){
    return  this.http.delete('http://localhost:8080/order/delete/'+id);
  }
  deleteWishlist(id:any){
    return this.http.delete('http://localhost:8080/wishlist/deletewishlist/'+id);
  }
  deleteCart(id:any){
   return this.http.delete('http://localhost:8080/cart/cart/'+id);
  }
}
