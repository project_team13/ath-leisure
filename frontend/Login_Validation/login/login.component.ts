import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpserviceService } from '../empservice.service';
import { Userdetails } from '../userdetails';
import { UserserviceService } from '../userservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user:Userdetails = new Userdetails();
  constructor(private userservice:UserserviceService){}
  userLogin(){ 
    console.log(this.user); 
    this.userservice.loginUser(this.user).subscribe({next:(d:any)=>
      alert("login successfull")
    });
} 
  ngOnInit(): void {
  }
}