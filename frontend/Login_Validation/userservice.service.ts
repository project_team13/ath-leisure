
import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Userdetails } from './userdetails';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {
  private baseurl="http://localhost:8089/user/login"
  constructor(private http:HttpClient) {}
  loginUser(user:Userdetails):Observable<object>{
       console.log(user);
       return this.http.post(`${this.baseurl}`,user);
  }
}
