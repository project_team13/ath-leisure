import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})                         
export class ResetPasswordComponent {
  code: string;
  password: string;                                                                        

  constructor(private route: ActivatedRoute, private http: HttpClient) {
    this.route.queryParams.subscribe(params => {
      this.code = params['code'];
    });
  }

  onSubmit() {
    this.http.post('/api/reset-password', { code: this.code, password: this.password }).subscribe(
      () => {
        alert('Your password has been reset.');
      },
      error => {
        alert(error.error.message);
      }
    );
  }
}

