import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterserviceService } from 'src/app/registerservice.service';


@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.css']
})
export class OtpComponent implements OnInit{
  user:any;
  Saved_OTP:any;
  form_OTP:any;
  otp=false;
  wotp=false;
  constructor(private userser:RegisterserviceService, private route:Router){
    this.user = JSON.parse(localStorage.getItem('user')|| '{}');
  }


  ngOnInit(): void {

  }

  sendOTP()
  {

    this.Saved_OTP = localStorage.getItem('OTP');
    if(this.Saved_OTP == this.form_OTP)
    {
     this.userser.addUser(this.user).subscribe((res)=>{

      this.otp=true;
      this.route.navigate(['/',"login"])
      .then(nav=>{
        localStorage.removeItem('OTP');
        localStorage.removeItem("user");
      }, err =>{
      });
     });
    }
    else{
      this.wotp=true;
      console.log("Invalid OTP");
    }
  }

}

