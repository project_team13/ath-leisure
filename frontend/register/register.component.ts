import { Component,OnInit } from '@angular/core';

import { FormControl, FormGroup ,Validators} from '@angular/forms';
import { RegisterserviceService } from '../registerservice.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user:any;
s:any;
  constructor(private userser:RegisterserviceService,private route:Router){


  }
  ngOnInit(): void {
   // throw new Error('Method not implemented.');
  }
registerForm=new FormGroup({
  name:new FormControl("",[Validators.required,Validators.minLength(2),Validators.pattern("[a-zA-Z].*")] ),
  address:new FormControl("",[Validators.required,Validators.minLength(2)] ),
  email:new FormControl("",[Validators.required,Validators.email]),
  mobile:new FormControl("",[Validators.required,Validators.pattern("[0-9]*"),Validators.minLength(10),Validators.maxLength(10)]),
pwd:new FormControl("",[Validators.required,Validators.maxLength(10)]),
cpwd:new FormControl("",[Validators.required])
});

registerSubmitted(){
  //console.log(this.registerForm.value);
  this.user={"uName":this.registerForm.get('name')?.value,
              "address":this.registerForm.get('address')?.value,
              "uEmail":this.registerForm.get('email')?.value,
              "uMobile":this.registerForm.get('mobile')?.value,
              "uPassword":this.registerForm.get('cpwd')?.value,

}
  console.log(this.user);


localStorage.setItem("user",JSON.stringify(this.user));
  console.log(localStorage.getItem('user'));


  this.s=JSON.parse(localStorage.getItem('user') || "");


  this.userser.validateUser(this.user).subscribe((res)=>{
    if(res==null){
      return;
    }
    this.route.navigate(['/',"otp"])
      .then(nav=>{
        console.log("success");
      }, err =>{
        console.log(err);
      });
      console.log("Enter OTP");
      console.log(res+"");
      localStorage.setItem("OTP", res+"");
      console.log("otp stored in local storage and it is :" + localStorage.getItem('OTP'));
  })
}

get Name():FormControl{
  return this.registerForm.get("name") as FormControl;
}
get Address():FormControl{
  return this.registerForm.get("address") as FormControl;
}
get Email():FormControl{
  return this.registerForm.get("email") as FormControl;
}
get Mobile():FormControl{
  return this.registerForm.get("mobile") as FormControl;
}
get pwd():FormControl{
  return this.registerForm.get("pwd") as FormControl;
}
}
