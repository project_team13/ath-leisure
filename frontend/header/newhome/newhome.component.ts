import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductCategoryService } from 'src/app/admin/product-category.service';
import { ProductService } from 'src/app/admin/product.service';
import { CartService } from 'src/app/cart.service';
import { UserService } from 'src/app/user.service';
import { WishlistService } from 'src/app/wishlist.service';

@Component({
  selector: 'app-newhome',
  templateUrl: './newhome.component.html',
  styleUrls: ['./newhome.component.css'],
})
export class NewhomeComponent implements OnInit {
  retrieved: any;
  name: any;
  loggedIn = false;
  categories: any[] = [];
  currentCategory: any;
  products: any;
  cartvalue: any;
  user: any;
  wishlist: any;
  searching: any;
  constructor(
    private route: Router,
    private pcatserv: ProductCategoryService,
    private pserv: ProductService,

    private userv: UserService
  ) {}
  ngOnInit(): void {
    this.loggedIn = this.userv.userLoggedIn();
    //  console.log(this.loggedIn);

    this.pserv.getProduct().subscribe((s: any) => {
      this.products = s;
    });
  }
  details(id: any) {
    localStorage.setItem('productid', id);
    this.route.navigateByUrl('productdetails');
  }
}
