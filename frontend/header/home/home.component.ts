import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
import { ProductCategoryService } from 'src/app/admin/product-category.service';
import { ProductService } from 'src/app/admin/product.service';
import { CartService } from 'src/app/cart.service';
import { UserService } from 'src/app/user.service';
import { WishlistService } from 'src/app/wishlist.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  retrieved: any;
  name: any;
  loggedIn = false;
  categories: any[] = [];
  currentCategory: any;
  products: any;
  cartvalue: any;
  user: any;
  usercart: any;
  userwish: any;
  wishlist: any;
  searching: any;
  myI;
  constructor(
    private route: Router,
    private pcatserv: ProductCategoryService,
    private pserv: ProductService,
    @Inject(DOCUMENT) private _document: Document,
    private userv: UserService,
    private cart: CartService,
    private wishlistserv: WishlistService
  ) {
    this.user = JSON.parse(localStorage.getItem('user')!);
  }
  ngOnInit(): void {
    this.loggedIn = this.userv.userLoggedIn();
    //  console.log(this.loggedIn);

    this.pserv.getProduct().subscribe((s: any) => {
      this.products = s;
    });
    this.getCatName();
    this.cart.getUserCart(this.user.uId).subscribe((s: any) => {
      this.usercart = s;
      console.log(s);
    });
    this.wishlistserv.getWishlist(this.user.uId).subscribe((s: any) => {
      this.userwish = s;
      console.log(s);
    });
  }
  // this._document.defaultView!.location.reload();
  getCatName() {
    this.pcatserv.getCatName().subscribe((d: any) => {
      this.categories = d;
      for (let i = 0; i < this.categories.length; i++) {
        if (this.categories[i] == this.name) {
          this.currentCategory = this.name;
        }
      }
    });
  }

  addToCart(p: any) {
    if (!this.userv.userLoggedIn()) {
      this.route.navigateByUrl('login');
      return;
    } else {
      //
      this.usercart.forEach((d: any) => {
        if (d.userId === this.user.uId && d.productId === p.productId) {
          Swal.fire('Already in Cart', '', 'warning');
          throw new Error('Item Present Already');
        }
      });
    }
    this.cartvalue = {
      productQuantity: 1,
      total: p.pPrice,
      productId: p.productId,
      userId: this.user.uId,
    };
    this.cart.addToCart(this.cartvalue).subscribe((s: any) => {
      console.log(s);
      Swal.fire('Added in Cart', '', 'success');
      setTimeout(function () {
        window.location.reload();
      }, 2000);
    });
  }

  addToWishList(p: any) {
    if (!this.loggedIn) {
      this.route.navigateByUrl('login');
      return;
    } else {
      this.userwish.forEach((d: any) => {
        if (d.userId === this.user.uId && d.productId === p.productId) {
          Swal.fire('Already in Wishlist', '', 'warning');
          throw new Error('Item Present Already');
        }
      });
    }
    this.wishlist = { productId: p.productId, userId: this.user.uId };
    this.wishlistserv.addtoWishlist(this.wishlist).subscribe((s: any) => {
      Swal.fire('Added in Wishlist', '', 'success');
      setTimeout(function () {
        window.location.reload();
      }, 1000);

      console.log(s);
    });
  }

  search(submit: any) {
    if (submit.search == '') {
      this.pserv.getProduct().subscribe((s: any) => {
        this.products = s;
      });
      return;
    }
    this.pserv.searchProductByName(submit.search).subscribe((s: any) => {
      this.products = s;
    });
  }

  details(id: any) {
    localStorage.setItem('productid', id);
    this.route.navigateByUrl('productdetails');
  }
}
