import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { DOCUMENT } from '@angular/common';
import { ProductCategoryService } from '../admin/product-category.service';
import { ProductService } from '../admin/product.service';
import { CartService } from '../cart.service';
import { WishlistService } from '../wishlist.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  loggedIn = false;
  catlist: any[] = [];
  searching: any = '';
  retrieved: any;
  name: any;
  categories: any[] = [];
  currentCategory: any;

  constructor(
    private route: Router,
    private pserv: ProductService,
    private pcatserv: ProductCategoryService,
    private userv: UserService,
    @Inject(DOCUMENT) private _document: Document,
    private cartserv: CartService,
    private wishlistserv: WishlistService
  ) {}
  ngOnInit(): void {
    localStorage.getItem('user');
    this.loggedIn = this.userv.userLoggedIn();
    this.pcatserv.getCatName().subscribe((s: any) => {
      this.catlist = s;
    });
  }

  logged() {
    if (this.loggedIn) {
      this.route.navigateByUrl('profile');
    } else {
      this.route.navigateByUrl('login');
    }
  }

  getProduct(item: any) {
    localStorage.setItem('item', JSON.stringify(item));
    this.route.navigateByUrl('/userproduct');
  }
  cart() {
    if (this.loggedIn) {
      this.route.navigateByUrl('/cart');
    } else {
      this.route.navigateByUrl('login');
    }
  }
  wishlist() {
    if (this.loggedIn) {
      this.route.navigateByUrl('/wishlist');
    } else {
      this.route.navigateByUrl('login');
    }
  }

  home() {
    this.route.navigateByUrl('/');
  }

  search(submit: any) {
    localStorage.removeItem('search');
    localStorage.setItem('search', submit);
    this.route.navigate(['/search', submit]);
  }
}
