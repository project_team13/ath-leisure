import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http:HttpClient) {

   }

   insertOrder(cart:any){
   return this.http.post('http://localhost:8080/order/insert/',cart);
   }
   updateOrder(cart:any){
    return this.http.put('http://localhost:8080/order/update/',cart);
   }
   getOrderbyUserId(id:any){
    return this.http.get('http://localhost:8080/order/display/'+id);
   }
   getOrder(){
    return this.http.get('http://localhost:8080/order/display/');
   }
   sendOrder(user:any){
    return this.http.post('http://localhost:8080/order/send/',user);
    }


}
