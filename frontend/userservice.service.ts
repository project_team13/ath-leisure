import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Userdetails } from './login/userdetails';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {
  private baseurl="http://localhost:8080/user/login"
  constructor(private http:HttpClient) {}
  loginUser(user){
       console.log(user);
       return this.http.post(`${this.baseurl}`,user);
  }
  getUserByEmail(uEmail:any){
    return this.http.get('http://localhost:8080/user/displayUserByEmail/'+uEmail);
  }

  resetPassword(user:any){
    return this.http.post('http://localhost:8080/user/forgetp/',user);
  }
}
