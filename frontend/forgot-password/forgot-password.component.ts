import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {
  email: string ;

  constructor(private http: HttpClient) {}

  onSubmit() {
    this.http.post('/api/forgot-password', { email: this.email }).subscribe(
      () => {
        alert('An email has been sent with instructions to reset your password.');
      },
      error => {
        alert(error.error.message);
      }
    );
  }
}
