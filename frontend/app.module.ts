import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { RegisterComponent } from './register/register.component';
import { AdminComponent } from './admin/admin.component';
import { AdminHeaderComponent } from './admin/admin-header/admin-header.component';
import { ProductsComponent } from './admin/products/products.component';
import { ProductCategoryComponent } from './admin/product-category/product-category.component';
import { OrdersComponent } from './admin/orders/orders.component';
import { UsersComponent } from './admin/users/users.component';
import { LoginComponent } from './login/login.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import {  HttpClientModule } from '@angular/common/http';
import { UserComponent } from './user/user.component';
import { ProfileComponent } from './user/profile/profile.component';
import { CartComponent } from './user/cart/cart.component';
import { WishlistComponent } from './user/wishlist/wishlist.component';
import { UserproductComponent } from './user/userproduct/userproduct.component';
import { InsertproductComponent } from './admin/products/insertproduct/insertproduct.component';
import { UpdateproductComponent } from './admin/products/updateproduct/updateproduct.component';
import { InsertProductCategoryComponent } from './admin/product-category/insert-product-category/insert-product-category.component';
import { NgxCaptchaModule } from 'ngx-captcha';
import { HomeComponent } from './header/home/home.component';
import { TotalPipe } from './user/cart/total.pipe';
import { SearchComponent } from './user/cart/search/search.component';
import { PaymentComponent } from './user/cart/payment/payment.component';
import { UserorderComponent } from './user/profile/userorder/userorder.component';
import { OtpComponent } from './register/otp/otp.component';
import { ForgotpasswordComponent } from './login/forgotpassword/forgotpassword.component';
import { FotpComponent } from './login/forgotpassword/fotp/fotp.component';
import { ResetpasswordComponent } from './login/forgotpassword/resetpassword/resetpassword.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RegisterComponent,
    AdminComponent,
    AdminHeaderComponent,
    ProductsComponent,
    ProductCategoryComponent,
    OrdersComponent,
    UsersComponent,
    LoginComponent,
    UserComponent,
    ProfileComponent,
    CartComponent,
    WishlistComponent,
    UserproductComponent,
    InsertproductComponent,
    UpdateproductComponent,
    InsertProductCategoryComponent,
    HomeComponent,
    TotalPipe,
    SearchComponent,
    PaymentComponent,
    UserorderComponent,
    OtpComponent,
    ForgotpasswordComponent,
    FotpComponent,
    ResetpasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,FormsModule,
    HttpClientModule,ReactiveFormsModule,NgxCaptchaModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
