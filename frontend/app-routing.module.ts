import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { ProductsComponent } from './admin/products/products.component';
import { ProductCategoryComponent } from './admin/product-category/product-category.component';
import { UsersComponent } from './admin/users/users.component';
import { OrdersComponent } from './admin/orders/orders.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { ProfileComponent } from './user/profile/profile.component';
import { CartComponent } from './user/cart/cart.component';
import { WishlistComponent } from './user/wishlist/wishlist.component';
import { UserproductComponent } from './user/userproduct/userproduct.component';
import { InsertproductComponent } from './admin/products/insertproduct/insertproduct.component';
import { UpdateproductComponent } from './admin/products/updateproduct/updateproduct.component';
import { InsertProductCategoryComponent } from './admin/product-category/insert-product-category/insert-product-category.component';
import { HomeComponent } from './header/home/home.component';
import { SearchComponent } from './user/cart/search/search.component';
import { PaymentComponent } from './user/cart/payment/payment.component';
import { UserorderComponent } from './user/profile/userorder/userorder.component';
import { OtpComponent } from './register/otp/otp.component';
import { ForgotpasswordComponent } from './login/forgotpassword/forgotpassword.component';
import { FotpComponent } from './login/forgotpassword/fotp/fotp.component';
import { ResetpasswordComponent } from './login/forgotpassword/resetpassword/resetpassword.component';
import { AdminGuard } from './admin/admin.guard';
import { UserGuard } from './user/user.guard';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'products', component: ProductsComponent ,canActivate:[AdminGuard]},
  { path: 'productcategory', component: ProductCategoryComponent ,canActivate:[AdminGuard]},
  { path: 'users', component: UsersComponent ,canActivate:[AdminGuard] },
  { path: 'orders', component: OrdersComponent,canActivate:[AdminGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'admin', component: AdminComponent ,canActivate:[AdminGuard]},
  { path: 'profile', component: ProfileComponent,canActivate:[UserGuard] },
  { path: 'cart', component: CartComponent },
  { path: 'wishlist', component: WishlistComponent },
  { path: 'search/:id', component: SearchComponent },
  { path: 'userproduct', component: UserproductComponent },
  { path: 'insertproduct', component: InsertproductComponent },
  { path: 'updateproduct', component: UpdateproductComponent },
  {
    path: 'insert-product-category',
    component: InsertProductCategoryComponent,canActivate:[AdminGuard]
  },
  {
    path:"payment" ,component:PaymentComponent,canActivate:[UserGuard]
  },
  {
    path:"userorder",component:UserorderComponent,canActivate:[UserGuard]
  },
  {path:"otp",component:OtpComponent},
  {path:"forgotpassword" ,component:ForgotpasswordComponent},
  {path:"fotp",component:FotpComponent},
  {path:"resetpassword",component:ResetpasswordComponent},
  { path: '**', pathMatch: 'full', component: HomeComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
