import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RegisterserviceService {



  constructor(private httpClient:HttpClient) {
  }

  getAllUsers()
  {
     return this.httpClient.get("http://localhost:8080/user/displayAllUsers");
  }
  addUser(user:any){
   return this.httpClient.post("http://localhost:8080/user/insertuser",user);
  }
  validateUser(user:any){
    return this.httpClient.post("http://localhost:8080/user/validateotp",user);
   }

   forgotUser(email:any){
    return this.httpClient.post("http://localhost:8080/user/forogtotp",email);
   }
}
