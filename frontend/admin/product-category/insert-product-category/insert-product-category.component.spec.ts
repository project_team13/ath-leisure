import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertProductCategoryComponent } from './insert-product-category.component';

describe('InsertProductCategoryComponent', () => {
  let component: InsertProductCategoryComponent;
  let fixture: ComponentFixture<InsertProductCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsertProductCategoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InsertProductCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
