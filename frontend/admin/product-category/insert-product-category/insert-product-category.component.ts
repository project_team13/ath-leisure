import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ProductCategoryService } from '../../product-category.service';

@Component({
  selector: 'app-insert-product-category',
  templateUrl: './insert-product-category.component.html',
  styleUrls: ['./insert-product-category.component.css']
})
export class InsertProductCategoryComponent {
constructor(private route:Router,private pcatserv:ProductCategoryService){

}
register(submit:any){
  this.pcatserv.addProductCat(submit).subscribe((s:any)=>{
  this.route.navigateByUrl("productcategory");
  });
  }
}
