import { Component,OnInit } from '@angular/core';
import { ProductCategoryService } from '../product-category.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-category',
  templateUrl: './product-category.component.html',
  styleUrls: ['./product-category.component.css']
})
export class ProductCategoryComponent  implements OnInit{
  productcat:any;
  constructor(private pserv:ProductCategoryService,private route:Router){

  }
  ngOnInit(): void {
      this.pserv.getProductCat().subscribe((s:any)=>{
        console.log(s);
        this.productcat=s;
      });
  }

  update(u:any){
  localStorage.setItem("productcat",JSON.stringify(u));
  this.route.navigateByUrl("updateproduct");
  }
  addProductCategory(){
    this.route.navigate(['insert-product-category']);
  }
  }
