import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  order: any;
  constructor() 
  { 
    this.order=[
      {orderId:101,userId:11,products:'ab',totalAmount:1000,paymentStatus:'success',orderstatus:'shipped'},
      {orderId:102,userId:12,products:'ab',totalAmount:100,paymentStatus:'success',orderstatus:'shipped'},
      {orderId:103,userId:13,products:'ab',totalAmount:500,paymentStatus:'Failed',orderstatus:'shipped'},
      {orderId:104,userId:14,products:'ab',totalAmount:800,paymentStatus:'success',orderstatus:'shipped'}
    ];
  }
  ngOnInit(): void {
  }
}
