import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductCategoryService {


  constructor(private http:HttpClient) {
  }

addProductCat(product:any){
  return this.http.post("http://localhost:8080/productcategory/list",product);
}
  getProductCat(){
    return this.http.get('http://localhost:8080/productcategory/list');
  }
  getUserByIdCat(id:any)
  {
     return this.http.get('http://localhost:8080/productcategory/list/'+id);
  }
  updateUserCat(product:any){
    return this.http.put('http://localhost:8080/productcategory/list/', product);
  }
  deleteProductCat(id:any){
  return this.http.delete('http://localhost:8080/productcategory/list/'+id);
  }
  getCatName(){
    return this.http.get('http://localhost:8080/productcategory/clist');
  }
}
