import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http:HttpClient) {
  }

addProduct(formData:FormData){
  return this.http.post("http://localhost:8080/product/insert",formData);
}
  getProduct(){
    return this.http.get('http://localhost:8080/product/list');
  }
  getProductById(id:any)
  {
     return this.http.get('http://localhost:8080/product/list/'+id);
  }
  updateUser(product:any){
    return this.http.put('http://localhost:8080/product/updateProduct/', product);
  }
  deleteProduct(id:any){
  return this.http.delete('http://localhost:8080/product/deleteProduct/'+id);
  }

  getProductByCategoryName(pName:any){
    return this.http.get('http://localhost:8080/product/category/'+pName);
  }
  /* deleteOrder(id:any){
    return  this.http.delete('http://localhost:8080/order/delete/'+id);
  }
  deleteWishlist(id:any){
    return this.http.delete('http://localhost:8080/wishlist/deletewishlist/'+id);
  }
  deleteCart(id:any){
   return this.http.delete('http://localhost:8080/cart/cart/'+id);
  } */

  searchProductByName(pName:any){
    return this.http.get('http://localhost:8080/product/productname/'+pName);
  }
  searchProductByDescription(pDescription:any){
    return this.http.get('http://localhost:8080/product/product/'+pDescription);
  }
}
