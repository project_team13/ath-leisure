import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent  implements OnInit{
products:any;
constructor(private pserv:ProductService,private route:Router){

}
ngOnInit(): void {
    this.pserv.getProduct().subscribe((s:any)=>{
      console.log(s);
      this.products=s;
    });
}

update(u:any){
localStorage.setItem("productdetails",JSON.stringify(u));
this.route.navigateByUrl("updateproduct");
}
addProduct(){
  this.route.navigate(['insertproduct']);
}
}
