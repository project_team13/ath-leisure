import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ProductCategoryService } from '../../product-category.service';
import { ProductService } from '../../product.service';

@Component({
  selector: 'app-updateproduct',
  templateUrl: './updateproduct.component.html',
  styleUrls: ['./updateproduct.component.css'],
})
export class UpdateproductComponent {
  retrieve: any;
  uproduct: any;
  constructor(
    private route: Router,
    private pcatserv: ProductCategoryService,
    private pserv: ProductService
  ) {
    this.retrieve = localStorage.getItem('productdetails');
    this.uproduct = JSON.parse(this.retrieve);
  }

  onUpdate(submit: any) {
    this.pserv.updateUser(submit).subscribe();
    localStorage.removeItem('productdetails');
    this.route.navigateByUrl('products');
  }
}
