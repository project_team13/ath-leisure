import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../../product.service';
import { ProductCategoryService } from '../../product-category.service';

@Component({
  selector: 'app-insertproduct',
  templateUrl: './insertproduct.component.html',
  styleUrls: ['./insertproduct.component.css'],
})
export class InsertproductComponent implements OnInit {
  pcat: any;
  userFile: any;
  public imagePath: any;
  imgURL: any;
  constructor(
    private route: Router,
    private pserv: ProductService,
    private pcatserv: ProductCategoryService
  ) {}
  ngOnInit(): void {
    this.pcatserv.getProductCat().subscribe((s: any) => {
      console.log(s);
      this.pcat = s;
    });
  }
  register(submit: any) {
    const formData = new FormData();
    const article = submit;
    formData.append('article', JSON.stringify(article));
    formData.append('file', this.userFile);
    this.pserv.addProduct(formData).subscribe((s: any) => {
      this.route.navigateByUrl('/products');
    });
  }

  onSelectFile(event: any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.userFile = file;
      // this.f['profile'].setValue(file);

      var mimeType = event.target.files[0].type;
      if (mimeType.match(/image\/*/) == null) {
        alert('Imager are only supported');

        return;
      }

      var reader = new FileReader();

      this.imagePath = file;
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        this.imgURL = reader.result;
      };
    }
  }
}
