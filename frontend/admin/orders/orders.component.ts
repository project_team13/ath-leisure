import { Component,OnInit } from '@angular/core';
import { OrderService } from 'src/app/order.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  orders: any;
  updateclicked=false;
  uorder: any;
  constructor(private orderserv:OrderService)
  {

  }
  ngOnInit(): void {
    this.orderserv.getOrder().subscribe((s:any)=>{

  this.orders=s;
    });
  }

  openPopup(c:any) {
   this.updateclicked=true;
    this.uorder=c;

  }
  closePopup() {
    this.updateclicked=false;
  }
  onUpdate(submit:any){

this.orderserv.updateOrder(submit).subscribe((s:any)=>{

  this.orderserv.getOrder().subscribe((s:any)=>{

this.orders=s;
  });
});
  }
}
